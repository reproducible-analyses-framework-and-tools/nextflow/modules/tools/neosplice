#!/usr/bin/env nextflow

include { extract_chroms_from_gff } from '../ref_utils/ref_utils.nf'

include { samtools_sort } from '../samtools/samtools.nf'
include { samtools_index } from '../samtools/samtools.nf'
include { samtools_merge } from '../samtools/samtools.nf'

include { combine_patient_samples } from '../utilities/utilities.nf'
include { combine_patient_samples as combine_patient_junctions } from '../utilities/utilities.nf'

include { clean_work_files as clean_fastas } from '../utilities/utilities.nf'
include { clean_work_files as clean_msbwt_tmps } from '../utilities/utilities.nf'
include { clean_work_files as clean_kmer_bams } from '../utilities/utilities.nf'
include { clean_work_dirs as clean_msbwts } from '../utilities/utilities.nf'

process neosplice_augmented_splice_graph_build {

  tag "${dataset}/${pat_name}/${tumor_run}"
  label 'neosplice_container'
  label 'neosplice_augmented_splice_graph_build'
  cache 'lenient'

  input:
  tuple val(pat_name), val(tumor_run), val(dataset), path(tumor_bam), path(tumor_bai), val(chr)
  path fa
  path gff
  val parstr

  output:
  tuple val(pat_name), val(dataset), val(tumor_run), path("*_graph.json"), emit: graph_jsons

  script:
  """
  CHR=`echo ${chr}`
  BAM_PLACEHOLDER=`echo ${tumor_bam}`
  FA_PLACEHOLDER=`echo ${fa}`
  samtools view -bS ${tumor_bam} \${CHR} > \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam
  samtools index \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam
  samtools faidx ${fa} \${CHR} > \${FA_PLACEHOLDER%.fa}.\${CHR}.fa
  python /NeoSplice/augmented_splice_graph.py build \
      --bam \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam \
      --genome \${FA_PLACEHOLDER%.fa}.\${CHR}.fa \
      --gff  ${gff} \
      --seq \${CHR} \
      --cpus 8 \
      --mem-per-cpu 3 \
      ${parstr}
  mv output/*json .
  rm -rf \${FA_PLACEHOLDER%.fa}.\${CHR}.fa
  rm -rf \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam
  """
}


process neosplice_get_max_kmer_length {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'neosplice_container'
  label 'neosplice_augmented_splice_graph_build'
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_bam), path(norm_bai), val(tumor_run), path(tumor_bam), path(tumor_bai)
  val parstr

  output:
  tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path("*max_kmer_len.txt"), emit: max_kmer_len

  script:
  """
  python /NeoSplice/get_max_kmer_length.py \
    --tumor_bam ${tumor_bam} \
    --normal_bam ${norm_bam} \
    ${parstr} \
    > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.max_kmer_len.txt
  """
}


process neosplice_convert_bam_to_fasta {

  tag "${dataset}/${pat_name}/${run}"
  label 'neosplice_container'
  label 'neosplice_convert_bam_to_fasta'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam), path(bai)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*_R1.fasta"), path("*_R2.fasta"), emit: fastas

  script:
  """
  python /NeoSplice/convert_bam_to_fasta.py \
  --bam_file ${bam} \
  --R1_out ${dataset}-${pat_name}-${run}_R1.fasta \
  --R2_out ${dataset}-${pat_name}-${run}_R2.fasta
  """
}


process neosplice_msbwtis {

  tag "${dataset}/${pat_name}/${run}"
  label 'neosplice_container'
  label 'neosplice_msbwtis'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*tmp"), emit: msbwt_tmps
  tuple val(pat_name), val(run), val(dataset), path("*tmp/*dat"), emit: deletable_intermediates

  script:
  """
  mkdir -p ${dataset}-${pat_name}-${run}.msbwt.tmp
  /msbwt-is/msbwtis ${dataset}-${pat_name}-${run}.msbwt.tmp ${fq1} ${fq2}
  """
}


process neosplice_convert_bwt_format {

  tag "${dataset}/${pat_name}/${run}"
  label 'neosplice_container'
  label 'neosplice_convert_bwt_format'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(msbwt_tmp)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*.msbwt"), emit: msbwts
  tuple val(pat_name), val(run), val(dataset), path("*.msbwt/*npy"), emit: deletable_intermediates

  script:
  """
  mkdir -p ${dataset}-${pat_name}-${run}.msbwt
  bash /NeoSplice/convert_BWT_format.bash ${msbwt_tmp} ${dataset}-${pat_name}-${run}.msbwt
  """
}


process neosplice_kmer_search_bwt {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'neosplice_container'
  label 'neosplice_kmer_search_bwt'
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_bwt), val(tumor_run), path(tumor_bwt), path(read_length)
  val parstr

  output:
  tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path("*.tumor_kmers.txt"), emit: tumor_kmers

  script:
  """
  max_kmer_read_len=`cat ${read_length}`
  python /NeoSplice/Kmer_search_bwt.py \
    --tumor_bwt ${tumor_bwt} \
    --normal_bwt ${norm_bwt} \
    --processors ${task.cpus} \
    --max_length \${max_kmer_read_len} \
    ${parstr} \
    --outdir ${dataset}-${pat_name}-${norm_run}_${tumor_run}.kmers/
  cat ${dataset}-${pat_name}-${norm_run}_${tumor_run}.kmers/Tumor_kmers_* >  ${dataset}-${pat_name}-${norm_run}_${tumor_run}.tumor_kmers.txt
  """
}


process neosplice_search_bam {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'neosplice_search_bam_container'
  label 'neosplice_search_bam'
  cache 'lenient'

  input:
  tuple val(pat_name), val(dataset), val(norm_run), val(tumor_run), path(kmers), path(bam), path(bai), val(chr)
  val parstr

  output:
  tuple val(pat_name), val(tumor_run), val(dataset), path("*tumor_kmer.chr*bam"), emit: kmer_bams

  script:
  """
  samtools view -bS ${bam} ${chr} > ${chr}.bam
  samtools index ${chr}.bam
  python /NeoSplice/search_bam.py \
  --Kmer_file ${kmers} \
  --input_bam_file ${chr}.bam \
  ${parstr} \
  --out_bam_file ${dataset}-${pat_name}-${norm_run}_${tumor_run}.tumor_kmer.${chr}.bam
  """
}


process neosplice_get_splice_junctions {

  tag "${dataset}/${pat_name}/${run}"
  label 'neosplice_container'
  label 'neosplice_get_splice_junctions'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam), path(bai)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*junctions.txt"), emit: junctions

  script:
  """
  python /NeoSplice/get_splice_junctions.py --input_bam ${bam} --out_file ${dataset}-${pat_name}-${run}.junctions.txt
  """
}


process neosplice_kmer_graph_inference {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'neosplice_container'
  label 'neosplice_kmer_graph_inference'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(tumor_bam), path(tumor_bai), path(tumor_kmer_bam), path(tumor_kmer_bai), path(norm_junctions), path(tumor_junctions), path(hla_calls), path(splice_graph)
  path gff
  path reference_fa
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("${dataset}-${pat_name}-*neoantigen_result"), emit: neoantigen_results

  script:
  """
  mkdir -p tmp
  export NETMHCpan=/netMHCpan-4.1-docker
  export TMPDIR=\${PWD}/tmp

  BAM_PLACEHOLDER=`echo ${tumor_bam}`
  KMER_BAM_PLACEHOLDER=`echo ${tumor_kmer_bam}`
  FA_PLACEHOLDER=`echo ${reference_fa}`

  HLA_ALLELES=`cat ${hla_calls}`
  for i in `ls *json`; do
    CHR=`echo \${i} | cut -f 1 -d '_'`;
    samtools view -bS ${tumor_bam} \${CHR} > \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam
    samtools index \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam
    samtools view -bS ${tumor_kmer_bam} \${CHR} > \${KMER_BAM_PLACEHOLDER%.bam}.\${CHR}.bam
    samtools index \${KMER_BAM_PLACEHOLDER%.bam}.\${CHR}.bam
    samtools faidx ${reference_fa} \${CHR} > \${FA_PLACEHOLDER%.fa}.\${CHR}.fa
    python /NeoSplice/kmer_graph_inference.py --sample ${dataset}-${pat_name}-${norm_run}_${tumor_run} \
      --chromosome \${CHR} \
      --bam_file \${BAM_PLACEHOLDER%.bam}.\${CHR}.bam \
      --gff_file ${gff} \
      --genome_fasta \${FA_PLACEHOLDER%.fa}.\${CHR}.fa \
      --kmer_bam \${KMER_BAM_PLACEHOLDER%.bam}.\${CHR}.bam \
      --splice_graph \${i} \
      --tumor_junction_file ${tumor_junctions} \
      --normal_junction_file ${norm_junctions} \
      --HLA_I \${HLA_ALLELES} \
      --HLA_II None \
      --netMHCpan_path /netMHCpan-4.1-docker/netMHCpan \
      --netMHCIIpan_path /netMHCIIpan-3.2-docker/netMHCIIpan \
      ${parstr} \
      --outdir ${dataset}-${pat_name}-\${CHR}.neoantigen_result&
  done
  wait

  rm -rf \${PWD}/tmp
  """
}


workflow neosplice {
  take:
    manifest
    bams_bais
    fa
    gff
    hla_calls
    species
    neosplice_augmented_splice_graph_build_parameters
    neosplice_get_max_kmer_length_parameters
    neosplice_convert_bams_to_fasta_parameters
    neosplice_get_splice_junctions_parameters
    neosplice_msbwt_is_parameters
    neosplice_convert_bwt_format_parameters
    neosplice_kmer_search_bwt_parameters
    neosplice_search_bam_parameters
    samtools_sort_parameters
    samtools_index_parameters
    neosplice_kmer_graph_inference_parameters
  main:
    chrom_count = 1000
    if( species =~ /[Hh]uman|hs|HS|[Hh]omo/ ) {
        chrom_count = 25
    } else if( species =~ /[Mm]ouse|mm|MM|[Mm]us/ ) {
        chrom_count = 22
    }
    println chrom_count
    extract_chroms_from_gff(
      gff)
    bams_bais.filter{ it[1] =~ 'ar-' }.set{tumor_bam_bais}
    bams_bais.filter{ it[1] =~ 'nr-' }.set{norm_bam_bais}
    tumor_bam_bais
      .concat(norm_bam_bais)
      .set{ rna_bams }
    rna_bams
      .combine(extract_chroms_from_gff.out.chroms_list.splitText())
      .set{ rna_bams_w_chr }
    neosplice_augmented_splice_graph_build(
      rna_bams_w_chr.filter{ it[1] =~ 'ar-' },
      fa,
      gff,
      neosplice_augmented_splice_graph_build_parameters)
    neosplice_convert_bam_to_fasta(
      rna_bams,
      neosplice_convert_bams_to_fasta_parameters)
    neosplice_get_splice_junctions(
      rna_bams,
      neosplice_get_splice_junctions_parameters)
    neosplice_msbwtis(
      neosplice_convert_bam_to_fasta.out.fastas,
      neosplice_msbwt_is_parameters)
    //Clean FASTAs here...
    neosplice_convert_bam_to_fasta.out.fastas
      .join(neosplice_msbwtis.out.msbwt_tmps, by: [0, 1, 2])
      .flatten()
      .filter{ it =~ /.fasta$/ }
      .set{ fasta_done_signal }
    clean_fastas(
      fasta_done_signal)
    neosplice_convert_bwt_format(
      neosplice_msbwtis.out.msbwt_tmps,
      neosplice_convert_bwt_format_parameters)
    //Clean msbwt temps here...
    neosplice_msbwtis.out.deletable_intermediates
      .join(neosplice_convert_bwt_format.out.msbwts, by: [0, 1, 2])
      .flatten()
      .filter{ it =~ /dat$/ }
      .set{ msbwt_tmps_done_signal }
    clean_msbwt_tmps(
      msbwt_tmps_done_signal)
    tumor_bam_bais
      .combine(norm_bam_bais)
      .map{ [it[0], it[2], it[6], it[8], it[9], it[1], it[3], it[4]] }
      .set{ rna_bams }


    neosplice_get_max_kmer_length(
      rna_bams,
      neosplice_get_max_kmer_length_parameters)
    manifest.filter{ it[5] =~ 'TRUE' }.set{ norm_set }
    manifest.filter{ it[5] =~ 'FALSE' }.set{ tumor_set }
    neosplice_convert_bwt_format.out.msbwts
      .filter{ it[1] =~ 'nr-' }
      .map{ [it[0], it[2], it[1], it[3]] }
      .set{ norm_msbwts }
    neosplice_convert_bwt_format.out.msbwts
      .filter{ it[1] =~ 'ar-' }
      .map{ [it[0], it[2], it[1], it[3]] }
      .set{ tumor_msbwts }
    norm_msbwts
      .combine(tumor_msbwts)
      .map{ [it[4], it[5], it[2], it[6], it[3], it[7]] }
      .set{ norm_tumor_msbwts }
    norm_tumor_msbwts
      .join(neosplice_get_max_kmer_length.out.max_kmer_len, by: [0, 1, 2, 3])
      .map{ [it[0], it[1], it[2], it[4], it[3], it[5], it[6]] }
      .set{norm_tumor_msbwts_read_len }
    neosplice_kmer_search_bwt(
      norm_tumor_msbwts_read_len,
      neosplice_kmer_search_bwt_parameters)
//  Clean msbwts here...
    neosplice_convert_bwt_format.out.deletable_intermediates
      .join(neosplice_kmer_search_bwt.out.tumor_kmers.map{ [it[0], it[3], it[1], it[4]] }, by: [0, 1, 2])
      .flatten()
      .filter{ it =~ /npy$/ }
      .set{ msbwts_done_signal }
    clean_msbwts(
      msbwts_done_signal)
    rna_bams
      .map{ [it[0], it[1], it[2], it[5], it[6], it[7]] }
      .set{ search_bams_input_component }
    neosplice_kmer_search_bwt.out.tumor_kmers
      .join(search_bams_input_component, by: [0, 1, 2, 3])
      .set{ search_bam_inputs }
    search_bam_inputs
      .combine(extract_chroms_from_gff.out.chroms_list.splitCsv(header: false, sep: '\n'))
      .set{ search_bam_inputs_w_chr }
    neosplice_search_bam(
      search_bam_inputs_w_chr,
      neosplice_search_bam_parameters)
    neosplice_search_bam.out.kmer_bams
      .groupTuple(by: [0,1,2], size: chrom_count)
      .set{ search_bams_by_pat}
    samtools_merge(
      search_bams_by_pat,
      '')
    samtools_sort(
      samtools_merge.out.merged_bams,
//      neosplice_search_bam.out.kmer_bams,
      samtools_sort_parameters)
//    samtools_sort.out.bams
//      .join(neosplice_search_bam.out.kmer_bams, by: [0, 1, 2])
//      .flatten()
//      .filter{ it =~ /kmer.bam$/ }
//      .set{ kmer_bams_done_signal }
//    clean_kmer_bams(
//      kmer_bams_done_signal)

    samtools_index(
      samtools_sort.out.bams,
      samtools_index_parameters)
    bams_bais
      .join(samtools_index.out.bams_and_bais, by: [0, 1, 2])
      .set{ bams_and_kmer_bams }
    neosplice_get_splice_junctions.out.junctions
      .filter{ it[1] =~ 'nr-' }
      .map{ [it[0], it[2], it[1], it[3]] }
      .set{ norm_junctions }
    neosplice_get_splice_junctions.out.junctions
      .filter{ it[1] =~ 'ar-' }
      .map{ [it[0], it[2], it[1], it[3]] }
      .set{ tumor_junctions }
    norm_junctions
      .combine(tumor_junctions)
      .map{ [it[4], it[2], it[6], it[5], it[3], it[7]] }
      .set{ norm_tumor_junctions }
    bams_and_kmer_bams
      .join(norm_tumor_junctions, by: 0)
      .map{ [it[0], it[7], it[1], it[2], it[3], it[4], it[5], it[6], it[10], it[11]] }
      .set{ bams_and_kmer_bams_and_junctions }
    bams_and_kmer_bams_and_junctions
      .join(hla_calls, by: 0)
      .map{ [it[0], it[1], it[2], it[3], it[4], it[5], it[6], it[7], it[8], it[9], it[12]] }
      .set{ bams_and_kmer_bams_and_junctions_and_hla_alleles }
    bams_and_kmer_bams_and_junctions_and_hla_alleles
      .combine(neosplice_augmented_splice_graph_build.out.graph_jsons, by: [0])
      .map{ [it[0], it[1], it[2], it[3], it[4], it[5], it[6], it[7], it[8], it[9], it[10], it[13]] }
      .set{ bams_and_kmer_bams_and_junctions_and_hla_alleles_and_splice_graphs }
    neosplice_kmer_graph_inference(
      bams_and_kmer_bams_and_junctions_and_hla_alleles_and_splice_graphs,
      gff,
      fa,
      neosplice_kmer_graph_inference_parameters)
    neoantigen_results = neosplice_kmer_graph_inference.out.neoantigen_results
      .groupTuple(by: [0,1,2,3], size:chrom_count)
     .set{ neo_results_by_pat}

  emit:
    neoantigen_results = neo_results_by_pat
}

process neosplice_filter_and_summarize_variants {

  label "neosplice_container"
  label "neosplice_sv_summarization"
  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  cache 'lenient'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/neosplice_filter_and_summarize_variants"

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(outcomes_peptides)
  path peptidome_ref

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("SV*txt"), optional: true, emit: splice_summaries
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*splice.pep.fa"), optional:true, emit: splice_c1_peptides

  script:
  """
  mkdir -p outcome_peptides
  cp *result/*/*/* outcome_peptides
  python /NeoSplice/splice_variant_neoantigen_summarization_cs.py \
    --ref-dir ${peptidome_ref}/ \
    --outcome-peptides-dir outcome_peptides

  tail -n +2 SVAgI_*.txt > headerless.tsv
  IFS="\\t"
  while read line; do
    PEP=`echo \${line} | cut -f 1`;
    NT_SEQ=`echo \${line} | cut -f 2`;
    CHR=`echo \${line} | cut -f 3`;
    TUMOR_SPLICE=`echo \${line} | cut -f 7 | sed 's/ //g'`;
    STRAND=`echo \${line} | cut -f 8`;
    GENE=`echo \${line} | cut -f 9`;
    MIN_EXP=`echo \${line} | rev | cut -f 1 | rev`;
    CHKSUM=`echo "\${CHR}:\${TUMOR_SPLICE}" | md5sum | cut -f 1 | cut -c1-15`;

    echo ">\${CHKSUM}" >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.splice.pep.fa;
    echo ";antigen_source:SPLICE chromosome:\${CHR} tumor_splice:\${TUMOR_SPLICE} strand:\${STRAND} gene:\${GENE} neosplice_min_exp:\${MIN_EXP} nt_seq:\${NT_SEQ}" >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.splice.pep.fa;
    echo "\${PEP}" >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.splice.pep.fa;
  done < headerless.tsv
  """
}
